FROM alpine:latest

# Install .NET CLI dependencies
RUN apk add --no-cache git curl wget

RUN wget -q -O - https://releases.rancher.com/cli/v0.6.7/rancher-linux-amd64-v0.6.7.tar.gz | tar xvzf - \
    && mv rancher-v0.6.7/rancher /bin \
    && rm -rf rancher-v0.4.1
